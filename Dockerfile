FROM node:16 As appbuild
ENV SERVER_HOME=/usr/src/app/
WORKDIR $SERVER_HOME
COPY ./package*.json $SERVER_HOME
#RUN apk add --no-cache python3 bash make g++ openssl-dev && rm -rf /var/cache/>
RUN npm install npm@6.14.17 -g
RUN npm install typescript -g
RUN npm i node-rdkafka
COPY . $SERVER_HOME
RUN chown -R node:node /usr/src/app
USER node
EXPOSE 3004 50054
RUN npm run build
CMD node dist/bin/server.js

